#!/bin/bash
if [ ${1:0:1} = "s" ]
then
    if [ ${2:0:4} = "pass" ]
    then
        if [ "$3" = "172.17.0.3" ]
        then
            echo "Framed-IP-Address = 172.17.15.3, Reply-Message = 'OK'"
            exit 0
        else
            echo "Reply-Message = 'NO ip'"
            exit 0
        fi
    else
        echo "Reply-Message = 'NO pass'"
        exit 0
    fi
else
    echo "Reply-Message = 'NO login'"
    exit 0
fi
