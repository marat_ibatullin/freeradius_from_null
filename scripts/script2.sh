#!/bin/bash
if [ ${1:0:1} = "s" ]
then
    echo "Framed-IP-Address = 172.17.15.3, Reply-Message = '$1 OK'"
    exit 0
else
    echo "Reply-Message = 'NO login ${1:0:1} $2 $3'"
    exit 0
fi
