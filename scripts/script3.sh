#!/bin/bash
if [ ${1:0:1} = "s" ]
then
    if [ "$3" = "127.0.0.1" ]
    then
        if [ ${2:0:4} = "pass" ]
        then
           echo "Framed-IP-Address = 172.17.0.3, Reply-Message = 'OK'"
           exit 0
        else
           echo "Reply-Message = 'NO pass'"
           exit 1
        fi
    else
       echo "Reply-Message = 'NO ip"
       exit 1
    fi
else
    echo "Reply-Message = 'NO login'"
    exit 0
fi
